var container = document.getElementsByClassName('calculatorContainer')[0];

var valueArray = ['0', '.', '=', '+', '1', '2', '3', '-', '4', '5', '6', '*', '7', '8', '9', '/'];

var numOfCol = 0;
var numOfRow = 0;

for (var x = 0; x < valueArray.length; x++) {

    var buttonContainer = document.createElement('div');
    buttonContainer.className = 'calculatorButton boxShadowExtraLight';
    buttonContainer.setAttribute('value', valueArray[x]);
    buttonContainer.innerHTML = '<p value=' + valueArray[x] + '>' + valueArray[x] + '</p>';

    if (valueArray[x] === '+' || 
        valueArray[x] === '-' ||
        valueArray[x] === '/' ||
        valueArray[x] === '*' ||
        valueArray[x] === '=' ||
        valueArray[x] === '.') 
    {
        buttonContainer.className += ' functionChar blockedChar';
    }

    if (valueArray[x] === "=") {
        buttonContainer.className += ' equalChar';
    };
    if (valueArray[x] === "/") {
        buttonContainer.className += ' division';
    };

    container.appendChild(buttonContainer);

    buttonContainer.style.bottom = 2 + numOfRow * 2  + numOfRow * 15.5 + '%';
    buttonContainer.style.left = 2 + numOfCol * 2  + numOfCol * 22.5 + '%';
    
    if (numOfCol < 3) {
        numOfCol++
    } else {
        numOfRow++;
        numOfCol = 0;
    }
}

var btns = document.getElementsByClassName('calculatorButton');

for (var i = 0; i < btns.length; i++) {

	btns[i].addEventListener('click', function (event) {
        getValue(event.srcElement.getAttribute('value'));
        blockFunctionButtons(event.srcElement.getAttribute('value'));
    }, false);
}

var score;

function getValue(value) {

    if (value >= 0 && value <= 9 || value === '.') {
        if (score === null || score === undefined) {
            score = value;
        } else {
            score += value;
        }
        printOnScreen(score);
    } else if (value === '=') {
        finishOperation(score);
    } else {
        score += ' ' + value + ' ';
        printOnScreen(score);
    }
}

function blockFunctionButtons(check) {
    var btns =  document.getElementsByClassName('functionChar');

    if (check >= 0 && check <= 9) {
        for (var i = 0; i < btns.length; i++) {
            btns[i].classList.remove("blockedChar");
        }
    } else {
        for (var i = 0; i < btns.length; i++) {
            btns[i].classList.add("blockedChar");
        }
    }
    if (check == 0) {
        var divisionBtn = document.getElementsByClassName('division')[0];
        divisionBtn.classList.add("blockedChar");
    }
}

var scoreText = document.getElementById('scoreText');
var finishValue = document.getElementById('scoreValue');

function printOnScreen(score) {
    scoreText.innerHTML = score;
}

function finishOperation(scoreValue) {
    scoreText.innerHTML = '&nbsp';
    var value = scoreValue.split(' ');

    for (var i = 0; i < value.length; i++) {

        if (value[i] !== '+' && 
            value[i] !== '-' &&
            value[i] !== '/' &&
            value[i] !== '*') 
        {
            value[i] = parseFloat(value[i], 10); 
        }
    }
    
    while (value.includes('/') || value.includes('*')) {
        for (var i = 0; i < value.length; i++) {

            var prevValue = value[i-1];
            var nextValue = value[i+1];
            var check = false;

            switch(value[i]) {
                case '/':
                    var divisionValue = (prevValue / nextValue).toFixed(2);
                    value[i-1] = divisionValue;
                    value.splice(i, 2);
                    value[i-1] = parseFloat(value[i-1], 10);
                    check = true;
                    break;
                case '*':
                    var increaseValue = (prevValue * nextValue).toFixed(2);
                    value[i-1] = increaseValue;
                    value.splice(i, 2);
                    value[i-1] = parseFloat(value[i-1], 10);
                    check = true;
                    break;
                default:
                    break;
            }
            if (check) {
                break;
            }
        }
    }

    while (value.includes('+') || value.includes('-')) {
        for (var i = 0; i < value.length; i++) {

            var prevValue = value[i-1];
            var nextValue = value[i+1];
            var check = false;

            switch(value[i]) {
                case '+':
                    var additionValue = (prevValue + nextValue).toFixed(2);
                    value[i-1] = additionValue;
                    value.splice(i, 2);
                    value[i-1] = parseFloat(value[i-1], 10);
                    var check = true;
                    break;
                case '-':
                    var subtractionValue = (prevValue - nextValue).toFixed(2);
                    value[i-1] = subtractionValue;
                    value.splice(i, 2);
                    value[i-1] = parseFloat(value[i-1], 10);
                    var check = true;
                    break;
                default:
                    break;
            }
            if (check) {
                break;
            }
        }
    }

    score = null;
    scoreText.innerHTML = '&nbsp';
    finishValue.innerHTML = value;
}
